﻿using System;
using System.Linq;
using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.Entities
{
    [TestFixture]
    public class CustomerAddUnsettledBetTests
    {
        [Test]
        public void AddUnsettledBet_InvalidCustomerId_ThrowArgumentException()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var unsettledBet = UnsettledBet.Create(CustomerId.Create(101), eventId, participantId, 250, 1000);
            var exception = Assert.Throws<ArgumentException>(() => customer.AddUnsettledBet(unsettledBet));
            Assert.AreEqual("Invalid UnsettledBet[CustomerId 101] for Customer 100.", exception.Message);
        }

        [Test]
        public void AddUnsettledBet_NotExistUnsettledBet_AddToCustomerUnsettledBets()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var unsettledBet = UnsettledBet.Create(CustomerId.Create(100), eventId, participantId, 250, 1000);
            customer.AddUnsettledBet(unsettledBet);

            Assert.True(customer.UnsettledBets.Count == 1);
            Assert.True(customer.UnsettledBets.First() == unsettledBet);
        }

        [Test]
        public void AddUnsettledBet_ExistUnsettledBet_IgnoreCustomerUnsettledBetsRemainTheSame()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var stake = 250;
            var toWin = 1000;
            var unsettledBet = UnsettledBet.Create(CustomerId.Create(100), eventId, participantId, stake, toWin);
            customer.AddUnsettledBet(unsettledBet);
            var duplicateBet = UnsettledBet.Create(CustomerId.Create(100), eventId, participantId, 888, 999);
            customer.AddUnsettledBet(duplicateBet);

            Assert.True(customer.UnsettledBets.Count == 1);
            Assert.True(customer.UnsettledBets.First() == unsettledBet);
            Assert.True(customer.UnsettledBets.First().Stake == stake);
            Assert.True(customer.UnsettledBets.First().ToWin == toWin);
        }
    }
}