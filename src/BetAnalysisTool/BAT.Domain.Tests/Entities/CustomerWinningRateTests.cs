﻿using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.Entities
{
    [TestFixture]
    public class CustomerWinningRateTests
    {
        [Test]
        public void WinningRate_4timesWin_ReturnWinningRate3()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 1000);
            customer.AddSettledBet(settledBet);
            Assert.True(customer.WinningRate == 3);
        }

        [Test]
        public void WinningRate_SmallerThan2timesWin_ReturnWinningRateSmallerThan1()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 450);
            customer.AddSettledBet(settledBet);
            Assert.True(customer.WinningRate == 0.8m);
        }
    }
}
