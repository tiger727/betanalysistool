﻿using System;
using System.Linq;
using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.Entities
{
    [TestFixture]
    public class CustomerAddSettledBetTests
    {
        [Test]
        public void AddSettledBet_InvalidCustomerId_ThrowArgumentException()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(CustomerId.Create(101), eventId, participantId, 250, 1000);
            var exception = Assert.Throws<ArgumentException>(() => customer.AddSettledBet(settledBet));
            Assert.AreEqual("Invalid SettledBet[CustomerId 101] for Customer 100.", exception.Message);
        }

        [Test]
        public void AddSettledBet_NotExistSettledBet_AddToCustomerSettledBets()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(CustomerId.Create(100), eventId, participantId, 250, 1000);
            customer.AddSettledBet(settledBet);

            Assert.True(customer.SettledBets.Count == 1);
            Assert.True(customer.SettledBets.First() == settledBet);
        }

        [Test]
        public void AddSettledBet_ExistSettledBet_IgnoreCustomerSettledBetsRemainTheSame()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var stake = 250;
            var win = 1000;
            var settledBet = SettledBet.Create(CustomerId.Create(100), eventId, participantId, stake, win);
            customer.AddSettledBet(settledBet);
            var duplicateBet = SettledBet.Create(CustomerId.Create(100), eventId, participantId, 888, 999);
            customer.AddSettledBet(duplicateBet);

            Assert.True(customer.SettledBets.Count == 1);
            Assert.True(customer.SettledBets.First() == settledBet);
            Assert.True(customer.SettledBets.First().Stake == stake);
            Assert.True(customer.SettledBets.First().Win == win);
        }
    }
}
