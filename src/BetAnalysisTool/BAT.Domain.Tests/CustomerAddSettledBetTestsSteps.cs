﻿using System;
using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;
using FluentAssertions;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BAT.Domain.Tests
{
    [Binding]
    public class CustomerAddSettledBetTestsSteps
    {
        private CustomerId _customerId = CustomerId.Create(12345);
        private Customer _customer;
        

        [Given(@"An initial customer")]
        public void GivenAnInitialCustomer()
        {
            _customer = Customer.Create(_customerId);
        }
        
        [When(@"Add a settled bet")]
        public void WhenAddASettledBet()
        {
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var settledBet = SettledBet.Create(_customerId, eventId, participantId, 250, 1000);
            _customer.AddSettledBet(settledBet);
        }
        
        [Then(@"The customer should have (.*) bet")]
        public void ThenTheCustomerShouldHaveBet(int p0)
        {
            _customer.SettledBets.Should().HaveCount(1);
        }
    }
}
