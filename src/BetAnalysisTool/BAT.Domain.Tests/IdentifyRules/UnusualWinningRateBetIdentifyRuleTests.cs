﻿using BAT.Domain.Entities;
using BAT.Domain.IdentifyRules;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.IdentifyRules
{
    [TestFixture]
    public class UnusualWinningRateBetIdentifyRuleTests
    {
        [Test]
        public void IsUnusualWinningCustomer_WinRateBelow60Percent_ReturnFalse()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 375);
            customer.AddSettledBet(settledBet);
            var rule = new UnusualWinningCustomerIdentifyRule();
            Assert.False(rule.IsUnusualWinningCustomer(customer));
        }

        [Test]
        public void IsUnusualWinningCustomer_WinRateEqual60Percent_ReturnTrue()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 400);
            customer.AddSettledBet(settledBet);
            var rule = new UnusualWinningCustomerIdentifyRule();
            Assert.True(rule.IsUnusualWinningCustomer(customer));
        }

        [Test]
        public void IsUnusualWinningCustomer_WinRateAbove60Percent_ReturnTrue()
        {
            var customerId = CustomerId.Create(100);
            var eventId = EventId.Create(200);
            var participantId = ParticipantId.Create(300);
            var customer = Customer.Create(customerId);
            var settledBet = SettledBet.Create(customerId, eventId, participantId, 250, 450);
            customer.AddSettledBet(settledBet);
            var rule = new UnusualWinningCustomerIdentifyRule();
            Assert.True(rule.IsUnusualWinningCustomer(customer));
        }
    }
}
