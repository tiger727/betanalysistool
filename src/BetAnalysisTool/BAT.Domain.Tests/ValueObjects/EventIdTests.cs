﻿using System;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.ValueObjects
{
    [TestFixture]
    public class EventIdTests
    {
        [Test]
        public void Create_InvalidStringValue_ThrowArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => EventId.CreateFrom("AAA"));
            Assert.AreEqual("Invalid value for EventId", exception.Message);
        }

        [Test]
        public void Create_ValidStringValue_ReturnCustomerId()
        {
            var eventId = EventId.CreateFrom("111");
            Assert.IsTrue(eventId.Id == 111);
        }
    }
}