using System;
using BAT.Domain.ValueObjects;
using NUnit.Framework;

namespace BAT.Domain.Tests.ValueObjects
{
    [TestFixture]
    public class ParticipantIdTests
    {
        [Test]
        public void Create_InvalidStringValue_ThrowArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => ParticipantId.CreateFrom("AAA"));
            Assert.AreEqual("Invalid value for ParticipantId", exception.Message);
        }

        [Test]
        public void Create_ValidStringValue_ReturnCustomerId()
        {
            var participantId = ParticipantId.CreateFrom("111");
            Assert.IsTrue(participantId.Id == 111);
        }
    }
}