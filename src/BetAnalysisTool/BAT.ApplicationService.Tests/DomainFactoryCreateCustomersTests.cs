﻿using System.Collections.Generic;
using System.Linq;
using BAT.ApplicationService.Dtos;
using NUnit.Framework;

namespace BAT.ApplicationService.Tests
{
    public class DomainFactoryCreateCustomersTests
    {
        [Test]
        public void CreateCustomers_EmptyBetsData_ReturnEmptyCustomerList()
        {
            var settledBets = new List<SettledBetDto>();
            var unsettledBets = new List<UnsettledBetDto>();
            var customers = DomainFactory.CreateCustomers(settledBets, unsettledBets);

            Assert.True(customers.Count == 0);
        }

        [Test]
        public void CreateCustomers_NotEmptySettledBetsData_ReturnCustomerWithSettledBets()
        {
            var settledBets = new List<SettledBetDto>();
            var settledBetDto1 = new SettledBetDto { CustomerId = 1, EventId = 10, ParticipantId = 20, Stake = 250, Win = 500 };
            settledBets.Add(settledBetDto1);
            var settledBetDto2 = new SettledBetDto { CustomerId = 2, EventId = 100, ParticipantId = 200, Stake = 2500, Win = 5000 };
            settledBets.Add(settledBetDto2);
            var settledBetDto3 = new SettledBetDto { CustomerId = 1, EventId = 11, ParticipantId = 21, Stake = 251, Win = 501 };
            settledBets.Add(settledBetDto3);
            var settledBetDto4 = new SettledBetDto { CustomerId = 2, EventId = 101, ParticipantId = 201, Stake = 2501, Win = 5001 };
            settledBets.Add(settledBetDto4);
            var unsettledBets = new List<UnsettledBetDto>();
            var customers = DomainFactory.CreateCustomers(settledBets, unsettledBets);

            Assert.True(customers.Count == 2);
            var orderedCustomers = customers.OrderBy(x => x.Id.Id).ToList();
            Assert.True(orderedCustomers[0].SettledBets.Count == 2);
            Assert.True(orderedCustomers[0].SettledBets.Any(x=>x.CustomerId.Id == settledBetDto1.CustomerId && x.EventId.Id == settledBetDto1.EventId && x.ParticipantId.Id == settledBetDto1.ParticipantId));
            Assert.True(orderedCustomers[0].SettledBets.Any(x=>x.CustomerId.Id == settledBetDto3.CustomerId && x.EventId.Id == settledBetDto3.EventId && x.ParticipantId.Id == settledBetDto3.ParticipantId));
            Assert.True(orderedCustomers[1].SettledBets.Count == 2);
            Assert.True(orderedCustomers[1].SettledBets.Any(x => x.CustomerId.Id == settledBetDto2.CustomerId && x.EventId.Id == settledBetDto2.EventId && x.ParticipantId.Id == settledBetDto2.ParticipantId));
            Assert.True(orderedCustomers[1].SettledBets.Any(x => x.CustomerId.Id == settledBetDto4.CustomerId && x.EventId.Id == settledBetDto4.EventId && x.ParticipantId.Id == settledBetDto4.ParticipantId));
        }

        [Test]
        public void CreateCustomers_NotEmptyUnsettledBetsData_ReturnCustomerWithUnsettledBets()
        {
            var settledBets = new List<SettledBetDto>();
            
            var unsettledBets = new List<UnsettledBetDto>();

            var unsettledBetDto1 = new UnsettledBetDto { CustomerId = 1, EventId = 10, ParticipantId = 20, Stake = 250, ToWin = 500 };
            unsettledBets.Add(unsettledBetDto1);
            var unsettledBetDto2 = new UnsettledBetDto { CustomerId = 2, EventId = 100, ParticipantId = 200, Stake = 2500, ToWin = 5000 };
            unsettledBets.Add(unsettledBetDto2);
            var unsettledBetDto3 = new UnsettledBetDto { CustomerId = 1, EventId = 11, ParticipantId = 21, Stake = 251, ToWin = 501 };
            unsettledBets.Add(unsettledBetDto3);
            var unsettledBetDto4 = new UnsettledBetDto { CustomerId = 2, EventId = 101, ParticipantId = 201, Stake = 2501, ToWin = 5001 };
            unsettledBets.Add(unsettledBetDto4);

            var customers = DomainFactory.CreateCustomers(settledBets, unsettledBets);

            Assert.True(customers.Count == 2);
            var orderedCustomers = customers.OrderBy(x => x.Id.Id).ToList();
            Assert.True(orderedCustomers[0].UnsettledBets.Count == 2);
            Assert.True(orderedCustomers[0].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto1.CustomerId && x.EventId.Id == unsettledBetDto1.EventId && x.ParticipantId.Id == unsettledBetDto1.ParticipantId));
            Assert.True(orderedCustomers[0].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto3.CustomerId && x.EventId.Id == unsettledBetDto3.EventId && x.ParticipantId.Id == unsettledBetDto3.ParticipantId));
            Assert.True(orderedCustomers[1].UnsettledBets.Count == 2);
            Assert.True(orderedCustomers[1].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto2.CustomerId && x.EventId.Id == unsettledBetDto2.EventId && x.ParticipantId.Id == unsettledBetDto2.ParticipantId));
            Assert.True(orderedCustomers[1].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto4.CustomerId && x.EventId.Id == unsettledBetDto4.EventId && x.ParticipantId.Id == unsettledBetDto4.ParticipantId));
        }

        [Test]
        public void CreateCustomers_MixedBetsData_ReturnCustomerWithBothKindsOfBets()
        {
            var settledBets = new List<SettledBetDto>();
            var settledBetDto1 = new SettledBetDto { CustomerId = 1, EventId = 10, ParticipantId = 20, Stake = 250, Win = 500 };
            settledBets.Add(settledBetDto1);
            var settledBetDto2 = new SettledBetDto { CustomerId = 2, EventId = 100, ParticipantId = 200, Stake = 2500, Win = 5000 };
            settledBets.Add(settledBetDto2);
            var settledBetDto3 = new SettledBetDto { CustomerId = 1, EventId = 11, ParticipantId = 21, Stake = 251, Win = 501 };
            settledBets.Add(settledBetDto3);
            var settledBetDto4 = new SettledBetDto { CustomerId = 2, EventId = 101, ParticipantId = 201, Stake = 2501, Win = 5001 };
            settledBets.Add(settledBetDto4);

            var unsettledBets = new List<UnsettledBetDto>();

            var unsettledBetDto1 = new UnsettledBetDto { CustomerId = 1, EventId = 10, ParticipantId = 20, Stake = 250, ToWin = 500 };
            unsettledBets.Add(unsettledBetDto1);
            var unsettledBetDto2 = new UnsettledBetDto { CustomerId = 2, EventId = 100, ParticipantId = 200, Stake = 2500, ToWin = 5000 };
            unsettledBets.Add(unsettledBetDto2);
            var unsettledBetDto3 = new UnsettledBetDto { CustomerId = 1, EventId = 11, ParticipantId = 21, Stake = 251, ToWin = 501 };
            unsettledBets.Add(unsettledBetDto3);
            var unsettledBetDto4 = new UnsettledBetDto { CustomerId = 2, EventId = 101, ParticipantId = 201, Stake = 2501, ToWin = 5001 };
            unsettledBets.Add(unsettledBetDto4);

            var customers = DomainFactory.CreateCustomers(settledBets, unsettledBets);

            Assert.True(customers.Count == 2);
            var orderedCustomers = customers.OrderBy(x => x.Id.Id).ToList();

            Assert.True(orderedCustomers[0].SettledBets.Count == 2);
            Assert.True(orderedCustomers[0].SettledBets.Any(x => x.CustomerId.Id == settledBetDto1.CustomerId && x.EventId.Id == settledBetDto1.EventId && x.ParticipantId.Id == settledBetDto1.ParticipantId));
            Assert.True(orderedCustomers[0].SettledBets.Any(x => x.CustomerId.Id == settledBetDto3.CustomerId && x.EventId.Id == settledBetDto3.EventId && x.ParticipantId.Id == settledBetDto3.ParticipantId));

            Assert.True(orderedCustomers[0].UnsettledBets.Count == 2);
            Assert.True(orderedCustomers[0].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto1.CustomerId && x.EventId.Id == unsettledBetDto1.EventId && x.ParticipantId.Id == unsettledBetDto1.ParticipantId));
            Assert.True(orderedCustomers[0].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto3.CustomerId && x.EventId.Id == unsettledBetDto3.EventId && x.ParticipantId.Id == unsettledBetDto3.ParticipantId));

            Assert.True(orderedCustomers[1].SettledBets.Count == 2);
            Assert.True(orderedCustomers[1].SettledBets.Any(x => x.CustomerId.Id == settledBetDto2.CustomerId && x.EventId.Id == settledBetDto2.EventId && x.ParticipantId.Id == settledBetDto2.ParticipantId));
            Assert.True(orderedCustomers[1].SettledBets.Any(x => x.CustomerId.Id == settledBetDto4.CustomerId && x.EventId.Id == settledBetDto4.EventId && x.ParticipantId.Id == settledBetDto4.ParticipantId));
            
            Assert.True(orderedCustomers[1].UnsettledBets.Count == 2);
            Assert.True(orderedCustomers[1].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto2.CustomerId && x.EventId.Id == unsettledBetDto2.EventId && x.ParticipantId.Id == unsettledBetDto2.ParticipantId));
            Assert.True(orderedCustomers[1].UnsettledBets.Any(x => x.CustomerId.Id == unsettledBetDto4.CustomerId && x.EventId.Id == unsettledBetDto4.EventId && x.ParticipantId.Id == unsettledBetDto4.ParticipantId));
        }
    }
}
