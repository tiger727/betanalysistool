﻿using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.IdentifyRules.RiskBetIdentifyRules
{
    internal class HightToWinRiskBetIdentifyRule : RiskBetIdentifyRule
    {
        internal override RiskBet DetectRiskBets(Customer customer, RiskBet riskBet)
        {
            if (riskBet.UnsettledBet.ToWin >= 1000)
            {
                riskBet.RaiseRisk(RiskLevel.HighToWin);
            }

            NextRiskBetIdentifyRule?.DetectRiskBets(customer, riskBet);

            return riskBet;
        }
    }
}