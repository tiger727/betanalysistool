﻿using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.IdentifyRules.RiskBetIdentifyRules
{
    internal class UnusualRiskBetIdentifyRule : RiskBetIdentifyRule
    {
        internal override RiskBet DetectRiskBets(Customer customer, RiskBet riskBet)
        {
            if (riskBet.UnsettledBet.Stake >=  customer.AverageBet * 10 && riskBet.UnsettledBet.Stake < customer.AverageBet * 30)
            {
                riskBet.RaiseRisk(RiskLevel.UnusualStake);
            }
            else if (riskBet.UnsettledBet.Stake >= customer.AverageBet * 30)
            {
                riskBet.RaiseRisk(RiskLevel.HighlyUnusualStake);
            }

            NextRiskBetIdentifyRule?.DetectRiskBets(customer, riskBet);

            return riskBet;
        }
    }
}