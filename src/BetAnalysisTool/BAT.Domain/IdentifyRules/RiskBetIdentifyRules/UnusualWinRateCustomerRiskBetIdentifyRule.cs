using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.IdentifyRules.RiskBetIdentifyRules
{
    internal class UnusualWinRateCustomerRiskBetIdentifyRule : RiskBetIdentifyRule
    {
        internal override RiskBet DetectRiskBets(Customer customer, RiskBet riskBet)
        {
            var rule = new UnusualWinningCustomerIdentifyRule();
            if (rule.IsUnusualWinningCustomer(customer))
            {
                riskBet.RaiseRisk(RiskLevel.RiskCustomer);
            }

            NextRiskBetIdentifyRule?.DetectRiskBets(customer, riskBet);

            return riskBet;
        }
    }
}