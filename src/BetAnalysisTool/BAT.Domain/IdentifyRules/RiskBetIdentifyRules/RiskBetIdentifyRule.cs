﻿using System.Collections.Generic;
using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;

namespace BAT.Domain.IdentifyRules.RiskBetIdentifyRules
{
    internal abstract class RiskBetIdentifyRule
    {
        protected RiskBetIdentifyRule NextRiskBetIdentifyRule;
        internal abstract RiskBet DetectRiskBets(Customer customer, RiskBet riskBet);

        internal void SetNextRiskBetIdentifyRule(RiskBetIdentifyRule nextRiskBetIdentifyRule)
        {
            NextRiskBetIdentifyRule = nextRiskBetIdentifyRule;
        }
    }
}
