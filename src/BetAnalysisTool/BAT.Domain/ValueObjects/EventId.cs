﻿using System;

namespace BAT.Domain.ValueObjects
{
    public struct EventId : IEquatable<EventId>
    {
        public bool Equals(EventId other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is EventId && Equals((EventId) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static bool operator ==(EventId left, EventId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(EventId left, EventId right)
        {
            return !left.Equals(right);
        }

        private EventId(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }

        public static EventId CreateFrom(string idfield)
        {
            int id;
            if (!int.TryParse(idfield, out id))
                throw new ArgumentException("Invalid value for EventId");

            return Create(id);
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public static EventId Create(int id)
        {
            return new EventId(id);
        }
    }
}