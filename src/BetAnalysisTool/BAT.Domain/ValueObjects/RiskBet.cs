using BAT.Domain.Entities;

namespace BAT.Domain.ValueObjects
{
    public class RiskBet
    {
        private RiskBet(RiskLevel riskLevel, UnsettledBet unsettledBet)
        {
            RiskLevel = riskLevel;
            UnsettledBet = unsettledBet;
        }

        public RiskLevel RiskLevel { get; private set; }
        public UnsettledBet UnsettledBet { get; private set; }

        internal bool HasRisk
        {
            get
            {
                return RiskLevel != RiskLevel.None;
            }
        }

        internal void RaiseRisk(RiskLevel riskLevel)
        {
            RiskLevel = RiskLevel | riskLevel;
        }

        internal static RiskBet Create(UnsettledBet unsettledBet)
        {
            return new RiskBet(RiskLevel.None, unsettledBet);
        }
    }
}