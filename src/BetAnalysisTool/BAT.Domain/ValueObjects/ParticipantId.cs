﻿using System;

namespace BAT.Domain.ValueObjects
{
    public struct ParticipantId : IEquatable<ParticipantId>
    {
        public bool Equals(ParticipantId other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ParticipantId && Equals((ParticipantId) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static bool operator ==(ParticipantId left, ParticipantId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ParticipantId left, ParticipantId right)
        {
            return !left.Equals(right);
        }

        private ParticipantId(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
        public static ParticipantId CreateFrom(string idfield)
        {
            int id;
            if (!int.TryParse(idfield, out id))
                throw new ArgumentException("Invalid value for ParticipantId");

            return Create(id);
        }

        public override string ToString()
        {
            return Id.ToString();
        }
        public static ParticipantId Create(int id)
        {
            return new ParticipantId(id);
        }
    }
}