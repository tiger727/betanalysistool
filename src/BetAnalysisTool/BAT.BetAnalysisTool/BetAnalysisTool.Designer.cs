﻿namespace BAT.BetAnalysisTool
{
    partial class BetAnalysisTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._txtSettledBets = new System.Windows.Forms.TextBox();
            this._lblSettledBets = new System.Windows.Forms.Label();
            this._btnBrowseSettledBets = new System.Windows.Forms.Button();
            this._btnUnsettledBets = new System.Windows.Forms.Button();
            this._lblUnsettledBets = new System.Windows.Forms.Label();
            this._txtUnsettledBets = new System.Windows.Forms.TextBox();
            this._btnDetect = new System.Windows.Forms.Button();
            this._fodBetsData = new System.Windows.Forms.OpenFileDialog();
            this._bgwDetect = new System.ComponentModel.BackgroundWorker();
            this._dgvResult = new System.Windows.Forms.DataGridView();
            this._btnCancel = new System.Windows.Forms.Button();
            this._colCustomer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._colEvent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._colParticipant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._colStake = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._colToWin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // _txtSettledBets
            // 
            this._txtSettledBets.Location = new System.Drawing.Point(123, 23);
            this._txtSettledBets.Name = "_txtSettledBets";
            this._txtSettledBets.ReadOnly = true;
            this._txtSettledBets.Size = new System.Drawing.Size(284, 20);
            this._txtSettledBets.TabIndex = 0;
            // 
            // _lblSettledBets
            // 
            this._lblSettledBets.AutoSize = true;
            this._lblSettledBets.Location = new System.Drawing.Point(42, 26);
            this._lblSettledBets.Name = "_lblSettledBets";
            this._lblSettledBets.Size = new System.Drawing.Size(64, 13);
            this._lblSettledBets.TabIndex = 1;
            this._lblSettledBets.Text = "Settled Bets";
            // 
            // _btnBrowseSettledBets
            // 
            this._btnBrowseSettledBets.Location = new System.Drawing.Point(423, 21);
            this._btnBrowseSettledBets.Name = "_btnBrowseSettledBets";
            this._btnBrowseSettledBets.Size = new System.Drawing.Size(75, 23);
            this._btnBrowseSettledBets.TabIndex = 2;
            this._btnBrowseSettledBets.Text = "Browse";
            this._btnBrowseSettledBets.UseVisualStyleBackColor = true;
            this._btnBrowseSettledBets.Click += new System.EventHandler(this._btnBrowseSettledBets_Click);
            // 
            // _btnUnsettledBets
            // 
            this._btnUnsettledBets.Location = new System.Drawing.Point(423, 69);
            this._btnUnsettledBets.Name = "_btnUnsettledBets";
            this._btnUnsettledBets.Size = new System.Drawing.Size(75, 23);
            this._btnUnsettledBets.TabIndex = 5;
            this._btnUnsettledBets.Text = "Browse";
            this._btnUnsettledBets.UseVisualStyleBackColor = true;
            this._btnUnsettledBets.Click += new System.EventHandler(this._btnUnsettledBets_Click);
            // 
            // _lblUnsettledBets
            // 
            this._lblUnsettledBets.AutoSize = true;
            this._lblUnsettledBets.Location = new System.Drawing.Point(42, 74);
            this._lblUnsettledBets.Name = "_lblUnsettledBets";
            this._lblUnsettledBets.Size = new System.Drawing.Size(76, 13);
            this._lblUnsettledBets.TabIndex = 4;
            this._lblUnsettledBets.Text = "Unsettled Bets";
            // 
            // _txtUnsettledBets
            // 
            this._txtUnsettledBets.Location = new System.Drawing.Point(123, 71);
            this._txtUnsettledBets.Name = "_txtUnsettledBets";
            this._txtUnsettledBets.ReadOnly = true;
            this._txtUnsettledBets.Size = new System.Drawing.Size(284, 20);
            this._txtUnsettledBets.TabIndex = 3;
            // 
            // _btnDetect
            // 
            this._btnDetect.Location = new System.Drawing.Point(45, 121);
            this._btnDetect.Name = "_btnDetect";
            this._btnDetect.Size = new System.Drawing.Size(186, 23);
            this._btnDetect.TabIndex = 6;
            this._btnDetect.Text = "Detect";
            this._btnDetect.UseVisualStyleBackColor = true;
            this._btnDetect.Click += new System.EventHandler(this._btnDetect_Click);
            // 
            // _fodBetsData
            // 
            this._fodBetsData.Filter = "CSV files (*.csv)|*.csv";
            // 
            // _bgwDetect
            // 
            this._bgwDetect.WorkerSupportsCancellation = true;
            this._bgwDetect.DoWork += new System.ComponentModel.DoWorkEventHandler(this._bgwDetect_DoWork);
            this._bgwDetect.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this._bgwDetect_RunWorkerCompleted);
            // 
            // _dgvResult
            // 
            this._dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgvResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._colCustomer,
            this._colEvent,
            this._colParticipant,
            this._colStake,
            this._colToWin});
            this._dgvResult.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this._dgvResult.Location = new System.Drawing.Point(45, 162);
            this._dgvResult.Name = "_dgvResult";
            this._dgvResult.RowHeadersVisible = false;
            this._dgvResult.Size = new System.Drawing.Size(453, 295);
            this._dgvResult.TabIndex = 7;
            // 
            // _btnCancel
            // 
            this._btnCancel.Location = new System.Drawing.Point(312, 121);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(186, 23);
            this._btnCancel.TabIndex = 8;
            this._btnCancel.Text = "Cancel";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // _colCustomer
            // 
            this._colCustomer.HeaderText = "Customer";
            this._colCustomer.Name = "_colCustomer";
            this._colCustomer.ReadOnly = true;
            // 
            // _colEvent
            // 
            this._colEvent.HeaderText = "Event";
            this._colEvent.Name = "_colEvent";
            this._colEvent.ReadOnly = true;
            // 
            // _colParticipant
            // 
            this._colParticipant.HeaderText = "Participant";
            this._colParticipant.Name = "_colParticipant";
            this._colParticipant.ReadOnly = true;
            // 
            // _colStake
            // 
            this._colStake.HeaderText = "Stake";
            this._colStake.Name = "_colStake";
            this._colStake.ReadOnly = true;
            // 
            // _colToWin
            // 
            this._colToWin.HeaderText = "ToWin";
            this._colToWin.Name = "_colToWin";
            this._colToWin.ReadOnly = true;
            // 
            // BetAnalysisTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 469);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._dgvResult);
            this.Controls.Add(this._btnDetect);
            this.Controls.Add(this._btnUnsettledBets);
            this.Controls.Add(this._lblUnsettledBets);
            this.Controls.Add(this._txtUnsettledBets);
            this.Controls.Add(this._btnBrowseSettledBets);
            this.Controls.Add(this._lblSettledBets);
            this.Controls.Add(this._txtSettledBets);
            this.Name = "BetAnalysisTool";
            this.Text = "BetAnalysisTool";
            ((System.ComponentModel.ISupportInitialize)(this._dgvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _txtSettledBets;
        private System.Windows.Forms.Label _lblSettledBets;
        private System.Windows.Forms.Button _btnBrowseSettledBets;
        private System.Windows.Forms.Button _btnUnsettledBets;
        private System.Windows.Forms.Label _lblUnsettledBets;
        private System.Windows.Forms.TextBox _txtUnsettledBets;
        private System.Windows.Forms.Button _btnDetect;
        private System.Windows.Forms.OpenFileDialog _fodBetsData;
        private System.ComponentModel.BackgroundWorker _bgwDetect;
        private System.Windows.Forms.DataGridView _dgvResult;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn _colCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn _colEvent;
        private System.Windows.Forms.DataGridViewTextBoxColumn _colParticipant;
        private System.Windows.Forms.DataGridViewTextBoxColumn _colStake;
        private System.Windows.Forms.DataGridViewTextBoxColumn _colToWin;
    }
}

