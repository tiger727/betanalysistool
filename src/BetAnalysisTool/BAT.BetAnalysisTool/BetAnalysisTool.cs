﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using BAT.ApplicationService;
using BAT.ApplicationService.Dtos;
using BAT.BetAnalysisTool.Helpers;

namespace BAT.BetAnalysisTool
{
    public partial class BetAnalysisTool : Form
    {
        public BetAnalysisTool()
        {
            InitializeComponent();
        }

        private void _btnBrowseSettledBets_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK ==_fodBetsData.ShowDialog())
            {
                try
                {
                    _txtSettledBets.Text = _fodBetsData.FileName;
                }
                catch
                {
                    MessageBox.Show("Please select a settled bets data file！");
                }
            }
        }

        private void _btnUnsettledBets_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _fodBetsData.ShowDialog())
            {
                try
                {
                    _txtUnsettledBets.Text = _fodBetsData.FileName;
                }
                catch
                {
                    MessageBox.Show("Please select an unsettled bets data file！");
                }
            }
        }

        private void _btnDetect_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_txtSettledBets.Text))
            {
                MessageBox.Show("Please select a settled bets data file！");
                return;
            }

            if (string.IsNullOrWhiteSpace(_txtUnsettledBets.Text))
            {
                MessageBox.Show("Please select an unsettled bets data file！");
                return;
            }

            if (_bgwDetect.IsBusy != true)
            {
                _btnDetect.Enabled = false;

                _bgwDetect.RunWorkerAsync();
            }
        }

        private void _bgwDetect_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = Detect();
        }

        private void _bgwDetect_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _btnDetect.Enabled = true;
            var riskBets = (IList<RiskBetDTO>) e.Result;
            BindDataGrid(riskBets);
        }

        private IList<RiskBetDTO> Detect()
        {
            try
            {
                var csvParser = new CSVParser();

                var settledBetFile = string.Empty;
                _txtSettledBets.Invoke(new Action(() => settledBetFile = _txtSettledBets.Text));
                var settledBets = csvParser.Parse(new FileInfo(settledBetFile), new SettledBetDtoMap());

                var unsettledBetFile = string.Empty;
                _txtUnsettledBets.Invoke(new Action(() => unsettledBetFile = _txtUnsettledBets.Text));

                var unsettledBets = csvParser.Parse(new FileInfo(unsettledBetFile), new UnsettledBetDtoMap());
                var identifyRiskBetService = new IdentifyRiskBetService();
                return identifyRiskBetService.Detect(settledBets, unsettledBets);
            }
            finally
            {
                _btnDetect.Invoke(new Action(() => _btnDetect.Enabled = true));
            }
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            if (_bgwDetect.WorkerSupportsCancellation == true)
            {
                _bgwDetect.CancelAsync();
            }
        }

        private void BindDataGrid(IList<RiskBetDTO> riskBets)
        {
            _dgvResult.Rows.Clear();
            foreach (var riskBetDto in riskBets)
            {
                AddDataGridRow(riskBetDto);
            }

            _dgvResult.AutoResizeColumns();
        }

        private void AddDataGridRow(RiskBetDTO riskBet)
        {
            var index = _dgvResult.Rows.Add();

            _dgvResult.Rows[index].Cells[0].Value = riskBet.CustomerId;
            if((riskBet.RiskBetLevel& RiskBetLevel.RiskCustomer) == RiskBetLevel.RiskCustomer)
                _dgvResult.Rows[index].Cells[0].Style.BackColor = Color.Red;

            _dgvResult.Rows[index].Cells[1].Value = riskBet.EventId;
            _dgvResult.Rows[index].Cells[2].Value = riskBet.ParticipantId;

            _dgvResult.Rows[index].Cells[3].Value = riskBet.Stake;
            if ((riskBet.RiskBetLevel & RiskBetLevel.UnusualStake) == RiskBetLevel.UnusualStake)
                _dgvResult.Rows[index].Cells[3].Style.BackColor = Color.Red;
            else if ((riskBet.RiskBetLevel & RiskBetLevel.HighlyUnusualStake) == RiskBetLevel.HighlyUnusualStake)
                _dgvResult.Rows[index].Cells[3].Style.BackColor = Color.DarkRed;

            _dgvResult.Rows[index].Cells[4].Value = riskBet.ToWin;
            if ((riskBet.RiskBetLevel & RiskBetLevel.HighToWin) == RiskBetLevel.HighToWin)
                _dgvResult.Rows[index].Cells[4].Style.BackColor = Color.Red;
        }
    }
}
