﻿using System.Collections.Generic;
using System.Linq;
using BAT.ApplicationService.Dtos;
using BAT.Domain.Entities;
using BAT.Domain.ValueObjects;

namespace BAT.ApplicationService
{
    public class DomainFactory
    {
        public static IList<Customer> CreateCustomers(IList<SettledBetDto> settledBets, IList<UnsettledBetDto> unsettledBets)
        {
            var customers = new List<Customer>();

            foreach (var settledBetDto in settledBets)
            {
                var customerId = CustomerId.Create(settledBetDto.CustomerId);
                var customer = FindCustomer(customers, customerId);
                var settledBet = SettledBet.Create(customerId, EventId.Create(settledBetDto.EventId),
                    ParticipantId.Create(settledBetDto.ParticipantId), settledBetDto.Stake, settledBetDto.Win);
                customer.AddSettledBet(settledBet);
            }

            foreach (var unsettledBetDto in unsettledBets)
            {
                var customerId = CustomerId.Create(unsettledBetDto.CustomerId);
                var customer = FindCustomer(customers, customerId);
                var settledBet = UnsettledBet.Create(customerId, EventId.Create(unsettledBetDto.EventId),
                    ParticipantId.Create(unsettledBetDto.ParticipantId), unsettledBetDto.Stake, unsettledBetDto.ToWin);
                customer.AddUnsettledBet(settledBet);
            }

            return customers;
        }

        private static Customer FindCustomer(ICollection<Customer> customers, CustomerId customerId)
        {
            Customer customer;
            if (customers.Any(x => x.Id == customerId))
                customer = customers.First(x => x.Id == customerId);
            else
            {
                customer = Customer.Create(customerId);
                customers.Add(customer);
            }

            return customer;
        }
    }
}
