﻿using System.Collections.Generic;
using System.Linq;
using BAT.ApplicationService.Dtos;
using BAT.Domain.Services;

namespace BAT.ApplicationService
{
    public class IdentifyRiskBetService
    {
        public IList<RiskBetDTO> Detect(IList<SettledBetDto> settledBets, IList<UnsettledBetDto> unsettledBets)
        {
            var customers = DomainFactory.CreateCustomers(settledBets, unsettledBets);

            var riskBetIdentifyService = new RiskBetIdentifyService();
            var result = customers.SelectMany(x => riskBetIdentifyService.DetectRiskBets(x))
                .Select(x => new RiskBetDTO
                {
                    RiskBetLevel = (RiskBetLevel) x.RiskLevel,
                    CustomerId = x.UnsettledBet.CustomerId.Id,
                    EventId = x.UnsettledBet.EventId.Id,
                    ParticipantId = x.UnsettledBet.ParticipantId.Id,
                    Stake = x.UnsettledBet.Stake,
                    ToWin = x.UnsettledBet.ToWin
                })
                .AsParallel().ToList();
            return result;
        }
    }
}
