﻿using System;

namespace BAT.ApplicationService.Dtos
{
    [Flags]
    public enum RiskBetLevel
    {
        None = 0,
        RiskCustomer = 1,
        UnusualStake = 2,
        HighlyUnusualStake = 4,
        HighToWin = 8
    }
}
