﻿namespace BAT.ApplicationService.Dtos
{
    public class UnsettledBetDto
    {
        public int CustomerId { get; set; }
        public int EventId { get; set; }
        public int ParticipantId { get; set; }
        public decimal Stake { get; set; }
        public decimal ToWin { get; set; }
    }
}